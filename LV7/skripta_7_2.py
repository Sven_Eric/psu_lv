from keras.preprocessing.image import img_to_array
from keras.models import load_model
from matplotlib import pyplot as plt
from skimage.transform import resize
from skimage import color
import matplotlib.image as mpimg
import numpy as np
from tensorflow import keras

filename = '/home/profesor/Downloads/2021_05_18_0fw_Kleki.png'

img = mpimg.imread(filename)
img = color.rgb2gray(img)
img = resize(img, (28, 28))


plt.figure()
plt.imshow(img, cmap=plt.get_cmap('gray'))
plt.show()


img = img.reshape(1, 28, 28, 1)
img = img.astype('float32')


# TODO: ucitaj model
model = keras.models.load_model('my_model')


# TODO: napravi predikciju 


# TODO: ispis rezultat
print("------------------------")
print('Broj: ', np.argmax(model.predict(img)))
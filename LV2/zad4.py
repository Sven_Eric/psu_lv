import matplotlib.pyplot as plt
import numpy as np
import skimage.io

img = skimage.io.imread('tiger.png', as_gray=True)
plt.figure(1)
            #640, 960
height = img.shape[0]
width = img.shape[1]
for i in range(0, height):
    for j in range(0, width):
        img[i][j] = img[i][j] + 85
        if img[i][j] > 255:
            img[i][j] = 255
img90 = np.zeros((width, height))
for j in range(0, height):
    img90[:,height-j-1] =img[j,:]
#img1=np.transpose(img)
plt.imshow(img90, cmap='gray', vmin=0, vmax=255)    
plt.show()
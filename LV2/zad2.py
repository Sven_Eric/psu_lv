import numpy
import numpy as np
import matplotlib.pyplot as plt

polje=[]
np.random.seed(102)
for x in range(100):
    x=np.random.randint(1,7)
    polje.append(x)
mat = numpy.array(polje)
print(mat)

plt.hist(mat,bins=(6))
plt.ylim(ymin=0, ymax=100)
plt.title('Histogram')
plt.show()
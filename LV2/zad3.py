import numpy as np
from matplotlib import pyplot
import matplotlib.pyplot as plt

data = np.loadtxt(open("mtcars.csv", "rb"), usecols=(1,2,3,4,5,6), delimiter=",", skiprows=1)
print(data)
pyplot.scatter(data[:,0],data[:,3], c = 'b', s=data[:,2])
print(min(data[:,0]))
print(max(data[:,0]))
print(sum(data[:,0]))
polje=[]
for i in data[:,1]:
    if i>=6:
        polje.append(i)
print(min(polje))
print(max(polje))
print(sum(polje))
plt.show()
import numpy as np
from sklearn.datasets import fetch_openml
import joblib
import pickle
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier


X, y = fetch_openml('mnist_784', version=1, return_X_y=True, as_frame=False)


slika = X[19].reshape((28, 28))
plt.imshow(slika, cmap = 'gray')
plt.show()



# skaliraj podatke, train/test split
X = X / 255.
X_train, X_test = X[:60000], X[60000:]
y_train, y_test = y[:60000], y[60000:]



mlp = MLPClassifier(hidden_layer_sizes=(50,), max_iter=10, alpha=1e-4,solver='sgd', verbose=10, random_state=1, learning_rate_init=.1)
mlp.fit(X_train,y_train)
print("Training: %f" % mlp.score(X_train, y_train))
print("Test: %f" % mlp.score(X_test, y_test))


# TODO: evaluirajte izgradenu mrezu


# spremi mrezu na disk
filename = "NN_model.sav"
joblib.dump(mlp, filename)
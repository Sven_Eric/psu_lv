Ocjena = 1.1
while Ocjena <= 0.0 or Ocjena >= 1.0:
    Ocjena = float(input("Unesite ocjenu: "))
if Ocjena >= 0.9:
    print("A")
elif Ocjena >= 0.8:
    print("B")
elif Ocjena >= 0.7:
    print("C")
elif Ocjena >= 0.6:
    print("D")
elif Ocjena >= 0.0:
    print("F")